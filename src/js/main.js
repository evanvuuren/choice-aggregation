$( document ).ready(function()
{
    //Service that will show the search section when selected
    function tabFocusRestrictor(lastItem,firstItem)
    {
        $(lastItem).blur(function()
        {
            $(firstItem).focus();
        });
    }

    tabFocusRestrictor('#search', '#overlay-close');

    //When the search button is selected in mobile view
    $("#trigger-overlay-mobile").click(function ()
    {
        $(".x-searchform-overlay").addClass('in');
        $('#search').focus();
    });

    //When the search button is selected in desktop view
    $("#trigger-overlay").click(function()
    {
        $(".x-searchform-overlay").addClass('in');
        $('#search').focus();
    });

    //Removing the search overlay when closed
    $(".overlay-close").click(function()
    {
        $( ".x-searchform-overlay" ).removeClass('in');
    });

    //Service that will slide the hero copy text from the top once the button has been clicked
    $("#join-us").click(function()
    {
        $('#home').slideToggle("slide");

        if($(this).text() === "Reasons to Join Us")
            $(this).text("Close")
        else
            $(this).text("Reasons to Join Us");
    });

});