'use strict';

var gulp = require('gulp'),
    connect = require('gulp-connect');

gulp.task('webserver', function() {
    connect.server();
});

gulp.task('default', ['webserver']);

gulp.task('watch', function() {
    gulp.watch('src/views/*.html', ['html']);
})